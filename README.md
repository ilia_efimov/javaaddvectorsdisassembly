# JavaAddVectorsDisassembly

Compiled with:
-Xcomp -XX:+UnlockDiagnosticVMOptions -XX:CompileCommand=print,*Program.testMethod -XX:PrintAssemblyOptions=intel -XX:+PrintCompilation -XX:+LogCompilation

-Xcomp - компилирует все методы до запуска
-XX:CompileThreshold=1 - чтобы JIT-компилятор компилировал методы после одного вызова
-XX:+UnlockDiagnosticVMOptions - нужная опция, чтобы остальные -XX-опции распознавались
-XX:CompileCommand=print,*Program.testMethod - указываем, что нам нужен дизассемблированный листинг метода testMethod класса Program. Зачем тут звездочка, я не понял, может быть потом разберусь
-XX:PrintAssemblyOptions=intel - устанавливает синтаксис, в котором будет выведен дизассемблированный код. Интеловский синтаксис более удобочитаем, поскольку вокруг регистров отсутствуют проценты %eax%
-XX:+PrintCompilation - выводить список методов, которые скомпилированы в нативный код - для отладки того, что наш искомый метод будет скомпилирован
-XX:+LogCompilation - вывести лог компиляции в файл hotspot.log (по умолчанию)

