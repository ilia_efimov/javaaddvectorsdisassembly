package com.comp.ex;

import java.util.Arrays;


public class VectorsCalc {

..................

    public int[] addVectors(int[] a, int[] b) {
        if (a.length != b.length) {
            return null;
        }
        int[] result = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        return result;
    }
	
...................

}


// class version 52.0 (52)
// access flags 0x21
public class com/comp/ex/VectorsCalc {

  // compiled from: VectorsCalc.java

  // access flags 0x1
  public <init>()V
   L0
    LINENUMBER 7 L0
    ALOAD 0
    INVOKESPECIAL java/lang/Object.<init> ()V
   L1
    LINENUMBER 9 L1
    RETURN
   L2
    LOCALVARIABLE this Lcom/comp/ex/VectorsCalc; L0 L2 0
    MAXSTACK = 1
    MAXLOCALS = 1

  // access flags 0x1
  public addVectors([I[I)[I
   L0
    LINENUMBER 12 L0
    ALOAD 1
    ARRAYLENGTH
    ALOAD 2
    ARRAYLENGTH
    IF_ICMPEQ L1
   L2
    LINENUMBER 13 L2
    ACONST_NULL
    ARETURN
   L1
    LINENUMBER 15 L1
   FRAME SAME
    ALOAD 1
    ARRAYLENGTH
    NEWARRAY T_INT
    ASTORE 3
   L3
    LINENUMBER 16 L3
    ICONST_0
    ISTORE 4
   L4
   FRAME APPEND [[I I]
    ILOAD 4
    ALOAD 1
    ARRAYLENGTH
    IF_ICMPGE L5
   L6
    LINENUMBER 17 L6
    ALOAD 3
    ILOAD 4
    ALOAD 1
    ILOAD 4
    IALOAD
    ALOAD 2
    ILOAD 4
    IALOAD
    IADD
    IASTORE
   L7
    LINENUMBER 16 L7
    IINC 4 1
    GOTO L4
   L5
    LINENUMBER 19 L5
   FRAME CHOP 1
    ALOAD 3
    ARETURN
   L8
    LOCALVARIABLE i I L4 L5 4
    LOCALVARIABLE this Lcom/comp/ex/VectorsCalc; L0 L8 0
    LOCALVARIABLE a [I L0 L8 1
    LOCALVARIABLE b [I L0 L8 2
    LOCALVARIABLE result [I L3 L8 3
    MAXSTACK = 5
    MAXLOCALS = 5
}




Compiled method (c2)   10307 3407       4       com.comp.ex.VectorsCalc::addVectors (45 bytes)
 total in heap  [0x00000000025af450,0x00000000025afa58] = 1544
 relocation     [0x00000000025af570,0x00000000025af590] = 32
 main code      [0x00000000025af5a0,0x00000000025af7a0] = 512
 stub code      [0x00000000025af7a0,0x00000000025af7b8] = 24
 oops           [0x00000000025af7b8,0x00000000025af7c0] = 8
 metadata       [0x00000000025af7c0,0x00000000025af7d0] = 16
 scopes data    [0x00000000025af7d0,0x00000000025af850] = 128
 scopes pcs     [0x00000000025af850,0x00000000025afa20] = 464
 dependencies   [0x00000000025afa20,0x00000000025afa28] = 8
 handler table  [0x00000000025afa28,0x00000000025afa40] = 24
 nul chk table  [0x00000000025afa40,0x00000000025afa58] = 24
Decoding compiled method 0x00000000025af450:
Code:
[Entry Point]
[Constants]
  # {method} {0x0000000014131530} 'addVectors' '([I[I)[I' in 'com/comp/ex/VectorsCalc'
  # this:     rdx:rdx   = 'com/comp/ex/VectorsCalc'
  # parm0:    r8:r8     = '[I'
  # parm1:    r9:r9     = '[I'
  #           [sp+0x40]  (sp of caller)
  0x00000000025af5a0: mov    r10d,DWORD PTR [rdx+0x8]
  0x00000000025af5a4: shl    r10,0x3
  0x00000000025af5a8: cmp    rax,r10
  0x00000000025af5ab: jne    0x0000000002125f60  ;   {runtime_call}
  0x00000000025af5b1: data32 xchg ax,ax
  0x00000000025af5b4: nop    DWORD PTR [rax+rax*1+0x0]
  0x00000000025af5bc: data32 data32 xchg ax,ax
[Verified Entry Point]
  0x00000000025af5c0: mov    DWORD PTR [rsp-0x6000],eax
  0x00000000025af5c7: push   rbp
  0x00000000025af5c8: sub    rsp,0x30           ;*synchronization entry
                                                ; - com.comp.ex.VectorsCalc::addVectors@-1 (line 12)
												
;--------------------------------------- START -------------------------------------------											

  0x00000000025af5cc: mov    ebx,DWORD PTR [r8+0xc]  ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@1 (line 12)
                                                ; implicit exception: dispatches to 0x00000000025af761
  0x00000000025af5d0: mov    ebp,DWORD PTR [r9+0xc]  ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@3 (line 12)
                                                ; implicit exception: dispatches to 0x00000000025af76d
  0x00000000025af5d4: cmp    ebx,ebp
  0x00000000025af5d6: je     0x00000000025af5ea  ;*if_icmpeq
                                                ; - com.comp.ex.VectorsCalc::addVectors@4 (line 12)

  0x00000000025af5d8: xor    r10d,r10d          ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@3 (line 12)

  0x00000000025af5db: mov    rax,r10
  0x00000000025af5de: add    rsp,0x30
  0x00000000025af5e2: pop    rbp
  0x00000000025af5e3: test   DWORD PTR [rip+0xfffffffffdf00a17],eax        # 0x00000000004b0000
                                                ;   {poll_return}
  0x00000000025af5e9: ret    
  0x00000000025af5ea: cmp    ebx,0x40000
  0x00000000025af5f0: ja     0x00000000025af712
  0x00000000025af5f6: movsxd rcx,ebx
  0x00000000025af5f9: shl    rcx,0x2
  0x00000000025af5fd: add    rcx,0x17
  0x00000000025af601: mov    r11,rcx
  0x00000000025af604: and    r11,0xfffffffffffffff8
  0x00000000025af608: mov    r10,QWORD PTR [r15+0x60]
  0x00000000025af60c: mov    rdi,r10
  0x00000000025af60f: add    rdi,r11
  0x00000000025af612: cmp    rdi,QWORD PTR [r15+0x70]
  0x00000000025af616: jae    0x00000000025af712
  0x00000000025af61c: mov    QWORD PTR [r15+0x60],rdi
  0x00000000025af620: prefetchnta BYTE PTR [rdi+0x100]
  0x00000000025af627: mov    QWORD PTR [r10],0x1
  0x00000000025af62e: prefetchnta BYTE PTR [rdi+0x140]
  0x00000000025af635: mov    DWORD PTR [r10+0x8],0x2000016d
                                                ;   {metadata({type array int})}
  0x00000000025af63d: mov    DWORD PTR [r10+0xc],ebx
  0x00000000025af641: prefetchnta BYTE PTR [rdi+0x180]
  0x00000000025af648: mov    rdi,r10
  0x00000000025af64b: add    rdi,0x10
  0x00000000025af64f: shr    rcx,0x3
  0x00000000025af653: add    rcx,0xfffffffffffffffe
  0x00000000025af657: xor    rax,rax
  0x00000000025af65a: rep stos QWORD PTR es:[rdi],rax  ;*newarray
                                                ; - com.comp.ex.VectorsCalc::addVectors@11 (line 15)

  0x00000000025af65d: test   ebx,ebx
  0x00000000025af65f: jle    0x00000000025af5db  ;*if_icmpge
                                                ; - com.comp.ex.VectorsCalc::addVectors@21 (line 16)

  0x00000000025af665: test   ebx,ebx
  0x00000000025af667: jbe    0x00000000025af749
  0x00000000025af66d: mov    ecx,ebx
  0x00000000025af66f: dec    ecx
  0x00000000025af671: cmp    ecx,ebx
  0x00000000025af673: jae    0x00000000025af749
  0x00000000025af679: test   ebp,ebp
  0x00000000025af67b: jbe    0x00000000025af749
  0x00000000025af681: cmp    ecx,ebp
  0x00000000025af683: jae    0x00000000025af749
  0x00000000025af689: mov    r11d,r10d
  0x00000000025af68c: shr    r11d,0x2
  0x00000000025af690: and    r11d,0x3
  0x00000000025af694: xor    edx,edx
  0x00000000025af696: mov    edi,0xffffffff
  0x00000000025af69b: sub    edi,r11d
  0x00000000025af69e: and    edi,0x3
  0x00000000025af6a1: inc    edi
  0x00000000025af6a3: cmp    edi,ebx
  0x00000000025af6a5: cmovg  edi,ebx            ;*aload_3
                                                ; - com.comp.ex.VectorsCalc::addVectors@24 (line 17)

  0x00000000025af6a8: mov    esi,DWORD PTR [r9+rdx*4+0x10]
  0x00000000025af6ad: add    esi,DWORD PTR [r8+rdx*4+0x10]
  0x00000000025af6b2: mov    DWORD PTR [r10+rdx*4+0x10],esi
                                                ;*iastore
                                                ; - com.comp.ex.VectorsCalc::addVectors@36 (line 17)

  0x00000000025af6b7: inc    edx                ;*iinc
                                                ; - com.comp.ex.VectorsCalc::addVectors@37 (line 16)

  0x00000000025af6b9: cmp    edx,edi
  0x00000000025af6bb: jl     0x00000000025af6a8  ;*if_icmpge
                                                ; - com.comp.ex.VectorsCalc::addVectors@21 (line 16)

  0x00000000025af6bd: mov    edi,ebx
  0x00000000025af6bf: add    edi,0xfffffffd
  0x00000000025af6c2: mov    esi,0x80000000
  0x00000000025af6c7: cmp    ecx,edi
  0x00000000025af6c9: cmovl  edi,esi
  0x00000000025af6cc: cmp    edx,edi
  0x00000000025af6ce: jge    0x00000000025af6f0  ;*aload_3
                                                ; - com.comp.ex.VectorsCalc::addVectors@24 (line 17)

  0x00000000025af6d0: movdqu xmm0,XMMWORD PTR [r9+rdx*4+0x10]
  0x00000000025af6d7: movdqu xmm1,XMMWORD PTR [r8+rdx*4+0x10]
  0x00000000025af6de: paddd  xmm1,xmm0
  0x00000000025af6e2: movdqu XMMWORD PTR [r10+rdx*4+0x10],xmm1
                                                ;*iastore
                                                ; - com.comp.ex.VectorsCalc::addVectors@36 (line 17)

  0x00000000025af6e9: add    edx,0x4            ;*iinc
                                                ; - com.comp.ex.VectorsCalc::addVectors@37 (line 16)

  0x00000000025af6ec: cmp    edx,edi
  0x00000000025af6ee: jl     0x00000000025af6d0  ;*if_icmpge
                                                ; - com.comp.ex.VectorsCalc::addVectors@21 (line 16)

  0x00000000025af6f0: cmp    edx,ebx
  0x00000000025af6f2: jge    0x00000000025af5db  ;*aload_3
                                                ; - com.comp.ex.VectorsCalc::addVectors@24 (line 17)

  0x00000000025af6f8: mov    ecx,DWORD PTR [r9+rdx*4+0x10]
  0x00000000025af6fd: add    ecx,DWORD PTR [r8+rdx*4+0x10]
  0x00000000025af702: mov    DWORD PTR [r10+rdx*4+0x10],ecx
                                                ;*iastore
                                                ; - com.comp.ex.VectorsCalc::addVectors@36 (line 17)

  0x00000000025af707: inc    edx                ;*iinc
                                                ; - com.comp.ex.VectorsCalc::addVectors@37 (line 16)

  0x00000000025af709: cmp    edx,ebx
  0x00000000025af70b: jl     0x00000000025af6f8  ;*if_icmpge
                                                ; - com.comp.ex.VectorsCalc::addVectors@21 (line 16)

  0x00000000025af70d: jmp    0x00000000025af5db
  0x00000000025af712: mov    DWORD PTR [rsp+0x10],ebx
  0x00000000025af716: mov    QWORD PTR [rsp+0x8],r9
  0x00000000025af71b: mov    QWORD PTR [rsp],r8
  0x00000000025af71f: movabs rdx,0x100000b68    ;   {metadata({type array int})}
  0x00000000025af729: mov    r8d,ebx
  0x00000000025af72c: data32 xchg ax,ax
  0x00000000025af72f: call   0x000000000214e6a0  ; OopMap{[0]=Oop [8]=Oop off=404}
                                                ;*newarray
                                                ; - com.comp.ex.VectorsCalc::addVectors@11 (line 15)
                                                ;   {runtime_call}
  0x00000000025af734: mov    r8,QWORD PTR [rsp]
  0x00000000025af738: mov    r9,QWORD PTR [rsp+0x8]
  0x00000000025af73d: mov    ebx,DWORD PTR [rsp+0x10]
  0x00000000025af741: mov    r10,rax
  0x00000000025af744: jmp    0x00000000025af65d
  0x00000000025af749: mov    edx,0xffffff86
  0x00000000025af74e: mov    rbp,r8
  0x00000000025af751: mov    QWORD PTR [rsp],r9
  0x00000000025af755: mov    QWORD PTR [rsp+0x8],r10
  0x00000000025af75a: nop
  0x00000000025af75b: call   0x00000000021257a0  ; OopMap{rbp=Oop [0]=Oop [8]=Oop off=448}
                                                ;*aload_3
                                                ; - com.comp.ex.VectorsCalc::addVectors@24 (line 17)
                                                ;   {runtime_call}
  0x00000000025af760: int3                      ;*aload_3
                                                ; - com.comp.ex.VectorsCalc::addVectors@24 (line 17)

  0x00000000025af761: mov    edx,0xfffffff6
  0x00000000025af766: nop
  0x00000000025af767: call   0x00000000021257a0  ; OopMap{off=460}
                                                ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@1 (line 12)
                                                ;   {runtime_call}
  0x00000000025af76c: int3                      ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@1 (line 12)

  0x00000000025af76d: mov    edx,0xfffffff6
  0x00000000025af772: nop
  0x00000000025af773: call   0x00000000021257a0  ; OopMap{off=472}
                                                ;*arraylength
                                                ; - com.comp.ex.VectorsCalc::addVectors@3 (line 12)
                                                ;   {runtime_call}
  0x00000000025af778: int3                      ;*newarray
                                                ; - com.comp.ex.VectorsCalc::addVectors@11 (line 15)

  0x00000000025af779: mov    rdx,rax
  0x00000000025af77c: add    rsp,0x30
  0x00000000025af780: pop    rbp
  0x00000000025af781: jmp    0x00000000021516e0  ;*if_icmpge
                                                ; - com.comp.ex.VectorsCalc::addVectors@21 (line 16)
                                                ;   {runtime_call}
;--------------------------------------- FINISH -------------------------------------------												
  0x00000000025af786: hlt    
  0x00000000025af787: hlt    
  0x00000000025af788: hlt    
  0x00000000025af789: hlt    
  0x00000000025af78a: hlt    
  0x00000000025af78b: hlt    
  0x00000000025af78c: hlt    
  0x00000000025af78d: hlt    
  0x00000000025af78e: hlt    
  0x00000000025af78f: hlt    
  0x00000000025af790: hlt    
  0x00000000025af791: hlt    
  0x00000000025af792: hlt    
  0x00000000025af793: hlt    
  0x00000000025af794: hlt    
  0x00000000025af795: hlt    
  0x00000000025af796: hlt    
  0x00000000025af797: hlt    
  0x00000000025af798: hlt    
  0x00000000025af799: hlt    
  0x00000000025af79a: hlt    
  0x00000000025af79b: hlt    
  0x00000000025af79c: hlt    
  0x00000000025af79d: hlt    
  0x00000000025af79e: hlt    
  0x00000000025af79f: hlt    
[Exception Handler]
[Stub Code]
  0x00000000025af7a0: jmp    0x000000000214e860  ;   {no_reloc}
[Deopt Handler Code]
  0x00000000025af7a5: call   0x00000000025af7aa
  0x00000000025af7aa: sub    QWORD PTR [rsp],0x5
  0x00000000025af7af: jmp    0x0000000002127200  ;   {runtime_call}
  0x00000000025af7b4: hlt    
  0x00000000025af7b5: hlt    
  0x00000000025af7b6: hlt    
  0x00000000025af7b7: hlt    